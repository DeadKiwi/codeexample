//
//  DebtsViewController.h
//  LifeImpulse
//
//  Created by Tischenko Alex on 17.05.18.
//  Copyright © 2018 Tischenko Alex. All rights reserved.
//

#import "BaseViewController.h"
#import "FinancesInfoDelegate.h"

@interface DebtsViewController : BaseViewController
@property (weak, nonatomic) id<FinancesInfoDelegate> financesDelegate;

@end
