//
//  DebtsViewModel.m
//  LifeImpulse
//
//  Created by Tischenko Alex on 22.05.18.
//  Copyright © 2018 Tischenko Alex. All rights reserved.
//

#import "DebtsViewModel.h"
#import "UserManager.h"
#import "FinanceInfo+CoreDataClass.h"

@implementation DebtsViewModel {
    UserManager *userManager;
    FinanceInfo *financeInfo;
}


#pragma mark -
#pragma mark Init

- (id)init {
    self = [super init];
    if (self) {
        [self initialize];
    };
    return self;
}

- (void)initialize {
    userManager = UserManager.sharedInstance;
    self.willChangeUserSubject = RACSubject.subject;
    
    [self bindData];
}

- (void)fetchDataFromServer {
    [super fetchDataFromServer];
}

#pragma mark -
#pragma mark Bindings

- (void)bindData {
    [self rac_liftSelector:@selector(updateCurrentUser:) withSignals:RACObserve(userManager, currentUser), nil];
}

- (void)updateCurrentUser:(id)x {
    [self.willChangeUserSubject sendNext:_currentUser];
    self.currentUser = userManager.currentUser;
    if (self.currentUser) {
        [self bindUser:_currentUser];
    }
}

- (void)bindUser:(User*)user {
    RAC(self, debtsSet) = [[RACObserve(self.currentUser.financeInfo, goods) map:^id _Nullable(id  _Nullable value) {
        return self.currentUser.financeInfo.goods;

    }] takeUntil:self.willChangeUserSubject];
}

- (void)financeInfoChanged:(id)x {
    financeInfo = userManager.currentUser.financeInfo;
    if (financeInfo) {
        [self rac_liftSelector:@selector(goodsChanged:) withSignals:RACObserve(financeInfo, goods), nil];
    }
}

- (void)goodsChanged:(id)x {
    self.debtsSet = self.currentUser.financeInfo.goods;
}

@end
