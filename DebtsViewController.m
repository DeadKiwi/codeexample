//
//  DebtsViewController.m
//  LifeImpulse
//
//  Created by Tischenko Alex on 17.05.18.
//  Copyright © 2018 Tischenko Alex. All rights reserved.
//

#import "DebtsViewController.h"
#import "DebtsViewModel.h"
#import "Good+CoreDataClass.h"
#import "DebtsTableViewCell.h"

@interface DebtsViewController () {
    __weak IBOutlet UITableView *debtsTableView;
    DebtsViewModel *viewModel;
    NSArray<Good*> *sortedDebts;
    NSMutableArray *expandedIndexes;
    BOOL needToUpdate;
}

@end

static const CGFloat kDefaultDebtCellHeight = 140.0f;
static const CGFloat kOperationItemHeight = 40.0f;
static NSString *debtCellIdentifier = @"debtsTableViewCell";

@implementation DebtsViewController

#pragma mark -
#pragma mark VC Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self bindViewModel];

    debtsTableView.tableFooterView = UIView.new;
    needToUpdate = true;
    expandedIndexes = NSMutableArray.new;

    [debtsTableView addPullToRefreshWithActionHandler:^{
        [self refreshViewModel:viewModel];
    }];

    [debtsTableView.pullToRefreshView setTitle:loadingTip forState:SVPullToRefreshStateLoading];
    [debtsTableView.pullToRefreshView setTitle:triggeredTip forState:SVPullToRefreshStateTriggered];
    [debtsTableView.pullToRefreshView setTitle:stoppedTip forState:SVPullToRefreshStateStopped];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self refreshViewModel:viewModel];
}

#pragma mark -
#pragma mark ViewModel

- (void)bindViewModel {
    viewModel = DebtsViewModel.new;
    [self bindViewModel:viewModel];

    [self rac_liftSelector:@selector(setupTableView:) withSignals:[RACObserve(viewModel, debtsSet) ignore:nil], nil];
}

- (void)setupTableView:(id)x {
    sortedDebts = [viewModel.debtsSet.allObjects sortedArrayUsingComparator:^NSComparisonResult(Good *obj1, Good *obj2) {
        NSString *dateString1 = obj1.timeCreate;
        NSString *dateString2 = obj2.timeCreate;

        return [dateString2 compare:dateString1];
    }];

    [debtsTableView.pullToRefreshView stopAnimating];
    [debtsTableView reloadData];
}


#pragma mark -
#pragma mark TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return sortedDebts.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DebtsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:debtCellIdentifier forIndexPath:indexPath];
    cell.good = [sortedDebts objectAtIndex:indexPath.row];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([expandedIndexes containsObject:indexPath]) {
        return kDefaultDebtCellHeight + [sortedDebts objectAtIndex:indexPath.row].operations.count * kOperationItemHeight;
    }

    return kDefaultDebtCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([expandedIndexes containsObject:indexPath]) {
        [expandedIndexes removeObject:indexPath];
    } else {
        [expandedIndexes addObject:indexPath];
    }

    [tableView beginUpdates];
    [tableView endUpdates];
}

#pragma mark -
#pragma mark ScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (needToUpdate) {
        [self.financesDelegate setupTableViewHeightOffset:debtsTableView.contentOffset.y];
    }
}

@end

