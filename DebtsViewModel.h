//
//  DebtsViewModel.h
//  LifeImpulse
//
//  Created by Tischenko Alex on 22.05.18.
//  Copyright © 2018 Tischenko Alex. All rights reserved.
//

#import "BaseViewModel.h"

@interface DebtsViewModel : BaseViewModel
@property (strong, nonatomic) User *currentUser;
@property (strong, nonatomic) RACSubject *willChangeUserSubject;
@property (strong, nonatomic) NSSet *debtsSet;

@end
